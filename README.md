Role to create a S3 bucket with a dedicated user and policy, and expiration

Used mostly for backups on AWS. 

# Usage

```
---
- hosts: aws_controller
  roles:
  - role: s3_bucket_backup
    project: myproject
```

This will automatically create a user, a policy and a bucket, see `vars/` for the naming.

# Adding a prefix

Since S3 buckets need to be globally unique on AWS, per [AWS documentation](https://docs.aws.amazon.com/AmazonS3/latest/userguide/bucketnamingrules.html), the prefix used for the bucket can be changed using `prefix`

```
---
- hosts: aws_controller
  roles:
  - role: s3_bucket_backup
    project: myproject
    prefix: mydnsname
```

# Giving credentials

The role assume that the credentials are already configured, either with a appropriate config file, or using
`module_defaults`.
